import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    // tslint:disable-next-line: quotemark
    path: "customers",
    loadChildren: () =>
      import("./customers/customers.module").then(m => m.CustomersModule)
  },
  {
    path: "loans",
    loadChildren: () =>
      import("./loans/loans.module").then(
        m => m.LoansModule
      )
  },
  {
    path: "",
    redirectTo: "/customers",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
