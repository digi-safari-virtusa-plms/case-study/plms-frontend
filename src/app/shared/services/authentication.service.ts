import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginUser } from 'src/app/customers/models/loginuser.model';
import { flatMap, map } from "rxjs/operators";
import { LoginComponent } from 'src/app/customers/components/login/login.component';
import { BehaviorSubject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly AUTHENTICATION_SERVICE_API_URL: String =
  "http://localhost:3000/users"; 

  public loggedInUserSubject : BehaviorSubject<LoginUser>;

  constructor(private httpClient : HttpClient) { 
    this.loggedInUserSubject = new BehaviorSubject(new LoginUser());
  }

  isUserLoggedIn() : BehaviorSubject<LoginUser>{
    return this.loggedInUserSubject;
  }

  

  validateUser(loginUser : LoginUser) : Promise<LoginUser> {
    console.log("validateUser ", loginUser);
    return this.httpClient.get<Array<LoginUser>>(`${this.AUTHENTICATION_SERVICE_API_URL}`).pipe(map(customers => {
      let customer=  customers.find(customer => customer['email'] === loginUser.email && customer['password'] === loginUser.password);
      this.loggedInUserSubject.next(customer);
      return customer;
     } )).toPromise();
 
    /*return this.httpClient.get<Array<LoginUser>>(`${this.AUTHENTICATION_SERVICE_API_URL}`).pipe(
      map(customers => {
        let customer=  customers.find(customer => customer['email'] === loginUser.email && customer['password'] === loginUser.password);
        this.loggedInUserSubject.next(customer);
        return this.loggedInUserSubject;
      } ))*/

  //  return this.httpClient.get<Array<LoginUser>>(`${this.AUTHENTICATION_SERVICE_API_URL}`).pipe(map(customers => {
  //        return  customers.find(customer => customer['email'] === loginUser.email && customer['password'] === loginUser.password);
  //   } )).toPromise();



    // return new Promise((resolve,reject)=>{
    //   this.httpClient.get<Array<LoginComponent>>(`${this.AUTHENTICATION_SERVICE_API_URL}`).toPromise().then(customers =>{
    //     console.log('customers ::' , customers);
    //     let customer = customers.find(customer => customer['email'] === loginUser.email);
    //     console.log('customer', customer);
        
    //     if(customer !== undefined || customer !== null){
    //      return Promise.resolve(customer);
    //     }
    //     else{
    //       return Promise.reject(new Error('No user found with these details'));
    //     }
        
    //   })
    // })
    


  }


  /** Add Login Details in users object in db.json */
   addLoginDetails(userLoginDto) :Observable<LoginUser>{
     return this.httpClient.post<LoginUser>(`${this.AUTHENTICATION_SERVICE_API_URL}`,userLoginDto);
   }
}
