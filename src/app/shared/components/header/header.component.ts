import { Component, OnInit } from "@angular/core";
import { SessionStorageService } from '../../services/sessionstorage.service';
import { RouterService } from 'src/app/loans/services/router.service';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  customerNameFirstLetter: string = "";
  customerLoggedIn: boolean = false;
  loggedInCustomerName: string;

  constructor(private sessionStorageService : SessionStorageService,
    private routerService : RouterService) {
    this.customerNameFirstLetter = "M";
  }

  ngOnInit() {
    this.sessionStorageService.getSessionStorageData().subscribe(loggedInUser =>{
      console.log("Response from Behavior subject", loggedInUser);
      console.log("userid", loggedInUser['id']);
      
      if(loggedInUser['id']  !== undefined){
        this.customerLoggedIn = true;
        this.loggedInCustomerName = loggedInUser['name'];
        this.customerNameFirstLetter = this.loggedInCustomerName.charAt(0);
      }
      else{
        this.customerLoggedIn = false;
      }
    })
   /* this.authenticationService.isUserLoggedIn().subscribe(res =>{
     
      
    })*/
  }

  routeToLogin(){
    this.routerService.routeToLogin();
  }

  routeToSignup(){
    this.routerService.routeToSignUp();
  }
  signout(){
    this.sessionStorageService.removeSessionStorageData();
    this.routerService.routeToLogin();

  }
}
