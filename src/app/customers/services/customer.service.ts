import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Customer } from 'src/app/loans/models/customer.model';
import { Observable, throwError, from } from 'rxjs';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { tap, mergeMap, map, catchError } from 'rxjs/operators';
import { CustomerLoan } from 'src/app/loans/models/customerloan.model';
import { Loan } from 'src/app/loans/models/loan.model';
import { LoansService } from 'src/app/loans/services/loans.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private readonly CUSTOMERS_SERVICE_API_URL: string =
    "http://localhost:3000/customers";

    customerLoansList: Array<CustomerLoan>;
    customersList: Array<Customer>;

  constructor(private httpClient : HttpClient,
              private authenticationService : AuthenticationService, 
              private loanService : LoansService) { 

    this.customerLoansList = [];    
    this.getCustomers().subscribe(response => {
      this.customersList = response;
    });
  }

  getUpdatedCustomers(): Promise<Array<Customer>> {
    
    return this.getCustomers().pipe(map(response => {
       this.customersList = response;
       return this.customersList;
        })).toPromise();
 
 
     }

 getCustomers(): Observable<Array<Customer>>{
   return this.httpClient
     .get<Array<CustomerLoan>>(`${this.CUSTOMERS_SERVICE_API_URL}`)
     .pipe(catchError(this.handleErrorCustomerLoans));
 }

 getCustomerById(customerId : number) : Promise<Customer>{
  return this.httpClient
  .get<Customer>(`${this.CUSTOMERS_SERVICE_API_URL}/${customerId}`)
  .pipe(catchError(this.handleErrorCustomerLoans)).toPromise();
 }

 updateCustomer(customer : Customer) : Observable<Customer> {
  return this.httpClient.patch<Customer>(
    `${this.CUSTOMERS_SERVICE_API_URL}/${customer.id}`,
        customer
     ).pipe(catchError(this.handleErrorCustomerLoans)); 
 }

 getCustomerLoans(customerId : number): Observable<Array<CustomerLoan>> {
   return this.httpClient
     .get<Customer>(`${this.CUSTOMERS_SERVICE_API_URL}/${customerId}`)
     .pipe(
       map(customer => customer['appliedLoans']),
       catchError(this.handleErrorCustomerLoans));
 }


  signupCustomer(customer : Customer) : Observable<Customer>{
    console.log("Inside Customer Service", customer);   
    
        return this.httpClient.post<Customer>(this.CUSTOMERS_SERVICE_API_URL,customer).pipe(tap(customer =>{
           let userLoginDto = {custId: customer.id, customerName : customer.name, email : customer.email, password: customer.password,role : 'customer'}
         console.log('user dto', userLoginDto);
         
          this.addLoginDetails(userLoginDto).subscribe(response =>{
           console.log("Customer is successfully added to the login details");
            
          }, error =>{
            console.log("Error in adding customer to login details", error);
            
            throwError("User Login Details are not added...")
          })

        }));
      
   
    
      
  }

  checkCustomerAlreadyExists(email : string) :Promise<Customer> {
    console.log("Inside AuthService email", email);      
     return this.httpClient.get<Array<Customer>>(`${this.CUSTOMERS_SERVICE_API_URL}`).pipe(map(customers =>{
      let customer=  customers.find(customer => customer['email'] === email);      
      return customer;
    })).toPromise();
  }
    
    /*return this.httpClient.get<Array<Customer>>(`${this.CUSTOMERS_SERVICE_API_URL}`).pipe(map(customers => {
      let customer=  customers.find(customer => customer['email'] === email);      
      return customer;
     } )).toPromise();
    }*/

   addLoginDetails(userLoginDto) {
     return this.authenticationService.addLoginDetails(userLoginDto);
   }


   getCustomerFavLoansList(customerId: number) : Observable<Array<Loan>>{
    return this.httpClient.get<Array<Loan>>(`${this.CUSTOMERS_SERVICE_API_URL}/${customerId}`).pipe(
      map(customer => customer['favLoans']),
      catchError(this.handleErrorCustomerLoans)
    )
  }

  addLoanToCustomerFavList(customerId : number, loanToBeFavourited : Loan){
    /**
     * This method should get the updated customers list 
     * In this service, the customerlist is given only inside constructor, 
     * when a new customer is signedup and try to click on fav loan, the customerList doesn't contain the new customer
     * since it is getting only at constructor, that's why we are calling getUpdatedCustomers() to get the updated customer list
     * 
     */
    /*
    from(promise) converts Promise to Observable
    */

    /*this.getUpdatedCustomers().then(response =>{
      console.log('test', response);

    })*/
    // from(this.getUpdatedCustomers()).subscribe(res => {
    //   console.log('test', res);
      
    //   console.log(res)});
    
    return from(this.getUpdatedCustomers()).pipe(mergeMap(response =>{
      console.log('response', response);
      console.log('customerid', customerId);
      /* check whether the customerId is number or string*/ 
       let customerToBeUpdated = response.find(customer => customer.id === customerId);
           console.log("customer to be updated", customerToBeUpdated    );
           customerToBeUpdated.favLoans.push(loanToBeFavourited);
           console.log("customer to be updated", customerToBeUpdated    );
    
    return this.httpClient.patch<Customer>(
       `${this.CUSTOMERS_SERVICE_API_URL}/${customerId}`,
           customerToBeUpdated
        ).pipe(tap(customer =>{
          this.loanService.incrementLoanFavCount(loanToBeFavourited).subscribe(updatedLoan =>{
            console.log("Updated Loan after Incrementing fav count", updatedLoan);
            
          })
          
        }))

    })); 
    

  }

  checkCustomerAlreadyAppliedForLoan(loanId, customerId) : Promise<CustomerLoan> {
    return this.httpClient.get<Customer>(`${this.CUSTOMERS_SERVICE_API_URL}/${customerId}`).pipe(map(customer =>{
      let appliedLoan =  customer.appliedLoans.find(loan => loan.id === loanId);      
      return appliedLoan;
    })).toPromise();
  }

  removeLoanFromCustomerFavList(customerId : number, loanToBeUnFavourited : Loan): Observable<Customer>{
    let customer: Customer = this.customersList.find(customer => customer.id === customerId);
    let loanIndex = customer.favLoans.findIndex(loan => loan.id === loanToBeUnFavourited.id);
    customer.favLoans.splice(loanIndex,1);
    console.log("customer to be updated", customer    );
    
    return this.httpClient.patch<Customer>(
      `${this.CUSTOMERS_SERVICE_API_URL}/${customerId}`,
      customer
    ).pipe(tap(customer =>{
      this.loanService.decrementLoanFavCount(loanToBeUnFavourited).subscribe(updatedLoan =>{
        console.log("Updated Loan after decrementing fav count", updatedLoan);
        
      })
      
    }))
    ;
  }

  approveLoan(customer : Customer): Observable<Customer> {
    return this.httpClient.patch<Customer>(
      `${this.CUSTOMERS_SERVICE_API_URL}/${customer.id}`,
      customer
    );
  }
  rejectLoan(customer: Customer) : Observable<Customer> {
    return this.httpClient.patch<Customer>(
      `${this.CUSTOMERS_SERVICE_API_URL}/${customer.id}`,
      customer
    );
  }

  applyNewLoan(customerId : number , customerLoan : CustomerLoan) : Observable<Customer>{
    console.log("Inside Customer Service new loan", customerLoan);

    return from(this.getCustomerById(customerId)).pipe(mergeMap(customer =>{
      customer.appliedLoans.push(customerLoan);
      return this.updateCustomer(customer);
    }))
  

    
  }
  
  /* Error Handling */

   handleErrorCustomerLoans(error: HttpErrorResponse): Observable<any> {
    console.log("Error in Fetching Customer Loans From Backend -->", error);
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // client-side error
      // errorMessage = `Error: ${error.error.message}`;
      errorMessage = `Some Browser issue occured. Please try again..`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      if (error.status === 404) {
        errorMessage = "Invalid URL. Please try with Proper URL";
      } else if (error.status === 403) {
        errorMessage = "UnAuthorized User. Please try with proper credentials";
      } else {
        errorMessage = "Some internal error occurred. Please try again later..";
      }
    }

    return throwError(errorMessage);
  }


}
