import { CustomerLoan } from "../../models/customerloan.model";
import { Component, OnInit } from "@angular/core";
import { LoansService } from "../../services/loans.service";
import { SessionStorageService } from 'src/app/shared/services/sessionstorage.service';
import { RouterService } from '../../services/router.service';
import { CustomerService } from 'src/app/customers/services/customer.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: "app-customerloans",
  templateUrl: "./customerloans.component.html",
  styleUrls: ["./customerloans.component.css"]
})
export class CustomerloansComponent implements OnInit {
  public customerId: number;
  public customerLoansList: Array<CustomerLoan>;
  public errorMessage: string;  

  constructor(private customerService: CustomerService, 
              private sessionStorageService : SessionStorageService, 
              private routerService : RouterService,
              private snackBar: MatSnackBar) {}

  ngOnInit() {
    this.sessionStorageService.getSessionStorageData().subscribe(loggedInUser =>{
      console.log("Getting user session info", loggedInUser);
      console.log("userid", loggedInUser['id']); //This return string so convert to number
        this.customerId = parseInt(loggedInUser['id'],10); //convert string to number 
       this.customerService.getCustomerLoans(this.customerId).subscribe(customersLoanResponse => {
         this.customerLoansList = customersLoanResponse;
         if(this.customerLoansList.length === 0){

          this.snackBar.open("You haven't applied for any loans, Please apply loan first..", " ", {
            duration: 3000,
            verticalPosition: 'top'
          });
           this.routerService.routeToAllLoansList();
         }
    
     });

  });
  }

  getLoanStatusCss(customerLoan: CustomerLoan) {
    console.log("cusotmer loan", customerLoan["status"]);
    if (customerLoan["verificationStatus"] === "Verification In Progress") {
      return "verification-in-progress";
    } else if (
      customerLoan["verificationStatus"] === "Verification Successful"
    ) {
      return "approved";
    } else if (customerLoan["verificationStatus"] === "Verification Failed") {
      return "rejected";
    }
  }

  
  routeToLoansList(){
    this.routerService.routeToAllLoansList();
  }

  routeToCustomerLoansList(){
    this.routerService.routeToCustomerLoansList();
  }
  
}
