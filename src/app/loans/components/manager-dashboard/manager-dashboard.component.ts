import { Component, OnInit } from "@angular/core";
import { LoansService } from "../../services/loans.service";
import { CustomerLoan } from "../../models/customerloan.model";
import { Customer } from '../../models/customer.model';
import { CustomerService } from 'src/app/customers/services/customer.service';

@Component({
  selector: "app-manager-dashboard",
  templateUrl: "./manager-dashboard.component.html",
  styleUrls: ["./manager-dashboard.component.css"]
})
export class ManagerDashboardComponent implements OnInit {
  public customersList: Array<Customer>;
  public customersListFromBackend: Array<Customer>;
  public errorMessage: String;

  constructor( private customerService : CustomerService) {
    console.log("Inside ManagerDashboard");

    this.customersList = [];
  }

  ngOnInit() {
    console.log("OnInit ManagerDashboard");

    this.customerService.getCustomers().subscribe(
      customersResponse => {
        //this.customerLoansList = customerLoansResponse;
        this.customersListFromBackend = customersResponse;
        this.customersList = this.customersListFromBackend;
        console.log("Customers  List", this.customersList);
      },
      error => {
        console.log("Error Message From Loan Service", error);
        this.errorMessage = error;
      }
    );
  }

  getLoanStatusCss(customerLoan: CustomerLoan) {
    console.log("cusotmer loan", customerLoan["status"]);
    if (customerLoan["verificationStatus"] === "Verification In Progress") {
      return "verification-in-progress";
    } else if (
      customerLoan["verificationStatus"] === "Verification Successful"
    ) {
      return "approved";
    } else if (customerLoan["verificationStatus"] === "Verification Failed") {
      return "rejected";
    }
  }

  approveLoan( customerId: number, customerLoan: CustomerLoan) {
    let customer = this.customersList.find(customer => customer.id === customerId);
    let loanToBeUpdated : CustomerLoan = customer.appliedLoans.find(loan => loan.id === customerLoan.id);
    loanToBeUpdated.verificationStatus = "Verification Successful";
    loanToBeUpdated.status = "Approved";
    this.customerService
      .approveLoan(customer)
      .subscribe(customerResponse => {
        let index = this.customersList.findIndex(
          customer => customer.id === customerId
        );
        this.customersList[index] = customerResponse;
      });
  }
  rejectLoan(customerId: number, customerLoan: CustomerLoan) {
    let customer = this.customersList.find(customer => customer.id === customerId);
    let loanToBeUpdated : CustomerLoan = customer.appliedLoans.find(loan => loan.id === customerLoan.id);
    loanToBeUpdated.verificationStatus = "Verification Failed";
    loanToBeUpdated.status = "Rejected";
    this.customerService
      .approveLoan(customer)
      .subscribe(customerResponse => {
        let index = this.customersList.findIndex(
          customer => customer.id === customerId
        );
        this.customersList[index] = customerResponse;
      });
  }

  filterLoans(status: string) {
    this.customersList = this.customersListFromBackend;
    if (status !== "") {
      this.customersList = this.customersList
      .filter(customer => customer.appliedLoans
        .some(appliedLoan => appliedLoan.status === status)
      )
      .map(customer => {
        let list = Object.assign({}, customer, {'appliedLoans': customer.appliedLoans.filter(
          appliedLoan => appliedLoan.status === status
        )})
        return list;
      })
      
    }
  }
}
