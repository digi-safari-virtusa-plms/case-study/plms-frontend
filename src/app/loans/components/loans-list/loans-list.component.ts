import { Loan } from "./../../models/loan.model";
import { Component, OnInit } from "@angular/core";
import { LoansService } from "../../services/loans.service";
import { CustomerLoan } from '../../models/customerloan.model';
import { MatSnackBar, MatDialog } from '@angular/material';
import { SessionStorageService } from 'src/app/shared/services/sessionstorage.service';
import { CustomerService } from 'src/app/customers/services/customer.service';
import { RouterService } from '../../services/router.service';
import { ApplyLoanComponent } from '../apply-loan/apply-loan.component';
import { Customer } from '../../models/customer.model';

@Component({
  selector: "app-loans",
  templateUrl: "./loans-list.component.html",
  styleUrls: ["./loans-list.component.css"]
})
export class LoansListComponent implements OnInit {
  public loansList: Array<Loan>;
  public customerFavLoansList : Array<Loan>;
  public customerAppliedLoansList : Array<CustomerLoan>;
  public customerId : number;
  public errorMessage : string;
  public applyLoanButtonCSS : string;

  constructor(private loanService: LoansService,
              private sessionStorageService : SessionStorageService,
    private snackbar: MatSnackBar,
    private customerService : CustomerService,
    private routerService : RouterService,
    public applyLoanDialog: MatDialog) {    
    this.customerFavLoansList = [];
    this.customerAppliedLoansList = [];
    this.applyLoanButtonCSS = "apply-loan-button";
  }

  ngOnInit() {
    this.sessionStorageService.getSessionStorageData().subscribe(loggedInUser =>{
      console.log("Response from Behavior subject", loggedInUser);
      console.log("userid", loggedInUser['id']); //This return string so convert to number
        this.customerId = parseInt(loggedInUser['id'],10); //convert string to number 
       this.loanService.getLoansList().subscribe(response => {
      this.loansList = response;
      /* Get the Customer Favourite Loans List to update the Favourite icon */
      this.customerService.getCustomerFavLoansList(this.customerId).subscribe(customerFavLoansListResponse =>{
        this.customerFavLoansList = customerFavLoansListResponse;
        console.log("customer fav loans list", this.customerFavLoansList);        
      })  

      /* Get the Customer applied Loans List to disable Apply Loan button 
      this.customerService.getCustomerLoans(this.customerId).subscribe(customerAppliedLoansListResponse =>{
        this.customerAppliedLoansList = customerAppliedLoansListResponse;
        console.log("Customer Appplied Loans List :", this.customerAppliedLoansList);
        
      })*/
    });

  });

  }

  isCustomerFavouriteLoan(loanId : number) : boolean {
    let isFavourite : boolean = false;
    if(this.customerFavLoansList.length > 0){
    let favLoan = this.customerFavLoansList.find(loan => loan.id === loanId);
   // console.log("FAvourite Loan", favLoan);
    
    if(favLoan !== undefined)
    isFavourite  = true;
    }

    return isFavourite;
  }

  /*
   This method is to check whether the customer is already applied for the loan ,
   if he is already applied, we need to disable the button, but this is not working 
   due to some issues in Angular template
  */
  /*isCustomerAppliedLoan(loanId : number) : string {
    this.applyLoanButtonCSS = "apply-loan-button";
    if(this.customerAppliedLoansList.length > 0){
    let appliedLoan = this.customerAppliedLoansList.find(loan => loan.id === loanId);
    console.log("applied loan", appliedLoan);
    
    if(appliedLoan !== undefined)   
    this.applyLoanButtonCSS = "disabled";
    }

    return this.applyLoanButtonCSS;
  }
*/

  addLoanToCustomerFavList(loanToBeFavourited : Loan){
    console.log('userid', this.customerId);    
    loanToBeFavourited.favouriteCount++;
    
    this.customerService.addLoanToCustomerFavList(this.customerId,loanToBeFavourited).subscribe(response =>{
      this.customerFavLoansList.push(loanToBeFavourited);
      this.snackbar.open("successfully Added to Favourite List", " ", {
        duration: 1000,
        verticalPosition: 'top'
      });
    }, error =>{
      console.log("Error" , error);
      
      this.errorMessage = "Some internal error occured . Please try again..";
      loanToBeFavourited.favouriteCount--;
    })
    
  }

  removeLoanFromCustomerFavList(loanToBeUnFavourited : Loan){
    loanToBeUnFavourited.favouriteCount--;

    this.customerService.removeLoanFromCustomerFavList(this.customerId,loanToBeUnFavourited).subscribe(response =>{
      let index = this.customerFavLoansList.findIndex(loan => loan.id === loanToBeUnFavourited.id);
    this.customerFavLoansList.splice(index,1);
    console.log("updated customer fav list", this.customerFavLoansList);   
      this.snackbar.open("successfully Removed from Favourite List", " ", {
        duration: 1000,
        verticalPosition: 'top'
      });
    }, error =>{
      console.log("Error" , error);
      
      this.errorMessage = "Some internal error occured . Please try again..";
      loanToBeUnFavourited.favouriteCount--;
    })



    
   /* this.customerService.removeLoanFromUserFavList(this.customerId,this.customerFavLoansList).subscribe(response =>{
      this.snackbar.open("successfully Removed from Favourite List", " ", {
        duration: 1000,
        verticalPosition: 'top'
      });
    }) */
  }

  applyForLoan(loan : Loan){
    console.log("Loan to be applied..", loan);
    console.log("customer id ", this.customerId);
    this.errorMessage = "";
    this.checkCustomerAlreadyAppliedForLoan(loan.id, this.customerId).then(appliedLoan =>{
      console.log("Applied Loan", appliedLoan);
      
      if(appliedLoan === undefined){
        const dialogRef = this.applyLoanDialog.open(ApplyLoanComponent,{
          data: {loan : loan, customerId : this.customerId}
        });

        dialogRef.afterClosed().subscribe(result =>{
          if(result !== undefined){
            this.errorMessage = result['error'];
          }
        })
      }
      else{
        this.errorMessage = "You have already applied for this loan..";
        this.snackbar.open("You have already applied for this loan", " ", {
          duration: 1000,
          verticalPosition: 'top'
        });
      }
    })
    
    
  }

  checkCustomerAlreadyAppliedForLoan(loanId, customerId)  {
   return this.customerService.checkCustomerAlreadyAppliedForLoan(loanId,customerId).then(appliedLoan =>{           
      return appliedLoan;
    })
  }

  routeToLoansList(){
    this.routerService.routeToAllLoansList();
  }

  routeToCustomerLoansList(){
    this.routerService.routeToCustomerLoansList();
  }
  

}
