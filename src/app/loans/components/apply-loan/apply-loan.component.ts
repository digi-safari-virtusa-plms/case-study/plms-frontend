import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Loan } from '../../models/loan.model';
import { FormGroup, FormGroupDirective, FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from 'src/app/customers/services/customer.service';
import { CustomerLoan } from '../../models/customerloan.model';

@Component({
  selector: 'app-apply-loan',
  templateUrl: './apply-loan.component.html',
  styleUrls: ['./apply-loan.component.css']
})
export class ApplyLoanComponent implements OnInit {

  loanSelected : Loan;
  customerId : number;
  applyLoanForm : FormGroup;
  errorMessage : string;
  applyLoanButtonText :string;

@ViewChild(FormGroupDirective,{static:true})
  formGroupDirective : FormGroupDirective

  constructor(public dialogRef: MatDialogRef<ApplyLoanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public formBuilder:FormBuilder,
    public customerService : CustomerService,
    public snackBar : MatSnackBar) {


      this.loanSelected = data.loan;
      this.customerId = data.customerId;
      console.log("loan",this.loanSelected);
      console.log("customer id", this.customerId);
      
      this.applyLoanForm = this.formBuilder.group({
        loanAmount : ['',Validators.compose([Validators.required,Validators.min(this.loanSelected.minLoanAmount),Validators.max(this.loanSelected.maxLoanAmount)])],
        rateOfInterest : ['',Validators.compose([Validators.required,Validators.min(this.loanSelected.minInterestRate),Validators.max(this.loanSelected.maxInterestRate)])],
    });
      
    }

  ngOnInit(): void {
    this.applyLoanButtonText = "Apply Loan";
  }

  applyLoan(applyLoanForm : FormGroup){
    this.applyLoanButtonText = "Please Wait..."
   this.checkCustomerCreditScore(this.loanSelected.minCreditScore,this.customerId).then(res =>{
    console.log("res", res);
    let loanAmount = this.applyLoanForm.get('loanAmount').value;
    let rateOfInterest = this.applyLoanForm.get('rateOfInterest').value;
    let dateOfApplication = new Date().toISOString().slice(0,10);
    console.log('Date of applicaiton ', dateOfApplication);
    let newCustomerLoan = new CustomerLoan();
    newCustomerLoan.id = this.loanSelected.id;
    newCustomerLoan.bankName = this.loanSelected.bankName;
    newCustomerLoan.bankImageUrl = this.loanSelected.bankImageUrl;
    newCustomerLoan.processingFee = this.loanSelected.processingFee;
    newCustomerLoan.appliedLoanAmount = loanAmount;
    newCustomerLoan.rateOfInterest = rateOfInterest;
    newCustomerLoan.termLength = this.loanSelected.termLength;
    newCustomerLoan.creditScore = this.loanSelected.minCreditScore;
    newCustomerLoan.dateOfApplication = JSON.stringify(dateOfApplication);
    
    console.log("New Loan ", newCustomerLoan);
    
     
    if(res == true){
      this.customerService.applyNewLoan(this.customerId, newCustomerLoan).subscribe(customer =>{
        console.log("Customer after applying new loan", customer);
        this.snackBar.open("You have successfully applied for this loan", " ", {
          duration: 2000,
          verticalPosition: 'top'
        });
        this.dialogRef.close();
    
      })
    }
     else if(res == false){
      let errorMessage = "Your credit Score is less. Min Required Credit Score is : " + this.loanSelected.minCreditScore;
      this.dialogRef.close({error: errorMessage});
     }
   })
    
    
  }

  checkCustomerCreditScore(loanMinCrediScore, custId){
      return this.customerService.getCustomerById(custId).then(customer =>{
        if(customer.creditScore >= loanMinCrediScore){
          return Promise.resolve(true);
        }
        else {
          return Promise.resolve(false);
        }
      })
  }



}
