import { CustomerLoan } from "../../models/customerloan.model";
import { Component, OnInit } from "@angular/core";
import { LoansService } from "../../services/loans.service";

@Component({
  selector: "app-customer-loans-dashboard",
  templateUrl: "./customer-dashboard.component.html",
  styleUrls: ["./customer-dashboard.component.css"]
})
export class CustomerDashboardComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    console.log("CustomerDashboardComponent");
  }
}
