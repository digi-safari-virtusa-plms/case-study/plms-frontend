import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouterService {

  constructor(private router : Router) { }

  routeToCustomerLoansList(){
    this.router.navigate(['loans/customer/applied-loans']);
  }

  routeToLogin(){
    this.router.navigate(['customers/login']);
  }

  routeToSignUp(){
    this.router.navigate(['customers/signup'])
  }

  routeToAllLoansList(){
    this.router.navigate(['loans/loans-list']);
  }

  routeToManagerDashbaord(){
    this.router.navigate(['loans/manager/dashboard']);
  }
}
