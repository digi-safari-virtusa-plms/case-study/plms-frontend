import { CustomerloansComponent } from "./components/customerloans/customerloans.component";
import { ManagerDashboardComponent } from "./components/manager-dashboard/manager-dashboard.component";
import { NgModule, OnInit } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CustomerDashboardComponent } from "./components/customer-dashboard/customer-dashboard.component";
import { LoansComponent } from "./loans.component";
import { LoansListComponent } from "./components/loans-list/loans-list.component";

const routes: Routes = [
  {
    path: "",
    component: LoansComponent,
    children: [
      {
        path: "customer",
        component: CustomerDashboardComponent,
        children: [
          
          {
            path: "applied-loans",
            component: CustomerloansComponent
          },
          {
            path: "",
            redirectTo: "applied-loans",
            pathMatch: "full"
          }
        ]
      },
      {
        path: "loans-list",
        component: LoansListComponent
      },
      { path: "manager/dashboard", component: ManagerDashboardComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoansRoutingModule {}
