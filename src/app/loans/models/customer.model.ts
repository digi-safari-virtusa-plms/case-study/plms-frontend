import { CustomerLoan } from './customerloan.model';
import { Loan } from './loan.model';

export class Customer{
  public id: number;  
  public name: string;
  public email: string;
  public password : string;
  public contactNo : number;
  public profilePicUrl: String;
  public gender: string;
  public age: number;
  public designation: string;
  public company: string;
  public monthlySalary: number;
  public creditScore : number;
  public appliedLoans : Array<CustomerLoan>;
  public favLoans : Array<Loan>;
  
  constructor(){
    this.appliedLoans = [];
    this.favLoans = [];
  }
}